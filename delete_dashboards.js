var __ = require('underscore'),
    fs = require('fs'),
    Ducksnode = require('./lib/ducksnode'),
    mongoose = require('mongoose'),
    schemas = require('./schemas');

var config;
var ducksnode;
var db;

var Dashboard, Widget, Slot;

var initApp = function(){
    ducksnode = Ducksnode.create({
        api_key: config.ducks_key
    });

    db = mongoose.createConnection(config.mongo_uri);
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function callback(){
        // Sync remote dashboards with local...
        Dashboard = db.model('Dashboard', schemas.dashboardSchema);
        Widget = db.model('Widget', schemas.widgetSchema);
        Slot = db.model('Slot', schemas.slotSchema);
        ducksnode.getDashboards(function(err, dashs, code){
            __.each(dashs.data, function(elem){
                if (elem.slug !== 'main-dashboard'){
                    ducksnode.removeDashboard(elem.slug, function(err, response, body){
                        console.log(body);
                    });
                }
            });
        });
        var toRemove = [Dashboard, Widget, Slot];
        __.each(toRemove, function(elem){
            elem.remove({}, function(err){
                console.log('Removed', elem);
            })
        });
    });
}
var readConfig = function(callback){
    console.log("Reading config file");
    fs.readFile('config.json', 'utf8', function(err, data){
    if (err)
        return console.log(err);
    config = JSON.parse(data);
    callback();
    });
}

var addWidgets = function(dashboard, callback){
    __.each(config.widgets, function(elem){
        ducksnode.addWidget(elem.kind, elem.name, dashboard, false, elem.slots,function(err, body, code){
            callback(elem.id, err, body, code);
        });
    });
}

readConfig(initApp);