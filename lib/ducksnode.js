/*
 * ducksnode
 *
 * Ducksboard API wrapper on node.js
 * https://github.com/iloire/ducksnode
 *
 * Copyright (c) 2012 Iván Loire
 * Licensed under the MIT license.
 */

var api_url = 'push.ducksboard.com/values/';
var dashboard_api_url = 'app.ducksboard.com/api/dashboards/';
var widget_api_url = 'app.ducksboard.com/api/widgets/';

var __ = require('underscore');

var getOnResponse = function(callback){
	return function (err, response, body){
		if (callback) {
			if (response != undefined && response.statusCode != undefined)
				code = response.statusCode;
			else
				code = false;
			callback(err, body, code);
		}
	}
}

exports.create = function(options) {
	var request = options.request || require ('request');

	if (!options.api_key){
		throw 'api_key required';
	}

	var errors = {
		400: 'Bad request. Please check your data',
		401: 'Unauthorized: API KEY not valid',
		403: 'Forbidden: You are not allowed to access this specific resource',
		404: 'Not found',
		413: 'Request Entity Too Large',
		500: 'Server Error'
	};

	return {
		//----------------------------------------
		// Push data to Ducksboard server
		//----------------------------------------
		'push' : function (widget, data, callback){
			var url = 'https://' + options.api_key + ':x@' + api_url + widget;

			if (typeof data !== 'object'){ //accept both primitives or objects
				data = {value: data};
			}

			request.post({url: url, json: true, body: data}, getOnResponse(callback));
		},
		'getDashboards': function( callback ){
			var url = 'https://' + options.api_key + ':x@' + dashboard_api_url;
			request(url, function(err, response, body){
				if (callback) {
					callback (
						err ? errors[response.statusCode] : null, 
						JSON.parse(body), 
						response.statusCode);
				}
			});
		},
		'addDashboard': function(name, background, callback){
			var data = {
				'name': name,
				'background': background
			}
			var url = 'https://' + options.api_key + ':x@' + dashboard_api_url;
			request.post({url: url, json: true, body: data}, getOnResponse(callback));
		},
		'removeDashboard': function(slug, callback){
			var url = 'https://' + options.api_key + ':x@' + dashboard_api_url +slug;
			request.del({url: url, json: true}, getOnResponse(callback));
		},
		'getWidgets': function(callback){
			var url = 'https://' + options.api_key + ':x@' + widget_api_url;
			request.get({url: url, json: true}, getOnResponse(callback));
		},
		'addWidget': function( kind, title, dashboard, sound, slots, modname, callback){
			var url = 'https://' + options.api_key + ':x@' + widget_api_url;
			var data = {
				'widget': {
					'kind': kind,
					'title': title,
					'dashboard': dashboard,
					'sound': sound
				},
				'slots': {

				}
			};
			__.each(slots, function(elem, key){
				data['slots'][key] = {
					"subtitle": elem.subtitle+modname,
				}
				if (elem.color !== undefined)
					data['slots'][key]["color"] = elem.color
				if (elem.timespan !== undefined)
					data['slots'][key]["timespan"] = elem.timespan
			});
			request.post({url: url, json:true, body: data}, getOnResponse(callback));
		}
	};
};
