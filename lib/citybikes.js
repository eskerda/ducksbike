var api = "http://api.citybik.es/";
var networks_url = "networks.json";
var request = require('request');

module.exports.networks = function(onComplete, onError){
    request(api+networks_url, function(error, body, response){
        if (!error){
            onComplete(JSON.parse(response));
        } else {
            onError(body, response);
        }
    });
}

module.exports.stations = function(system, onComplete, onError){
    request(api+system+".json", function(error, body, response){
        if (!error){
            onComplete(JSON.parse(response));
        } else {
            onError(body, response);
        }
    });
}