var __ = require('underscore'),
    fs = require('fs'),
    Ducksnode = require('./lib/ducksnode'),
    mongoose = require('mongoose'),
    schemas = require('./schemas');

var config;
var ducksnode;
var db;

var Dashboard, Widget, Slot;

var initApp = function(){
    ducksnode = Ducksnode.create({
        api_key: config.ducks_key
    });

    db = mongoose.createConnection(config.mongo_uri);
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function callback(){
        // Sync remote dashboards with local...
        Dashboard = db.model('Dashboard', schemas.dashboardSchema);
        Widget = db.model('Widget', schemas.widgetSchema);
        Slot = db.model('Slot', schemas.slotSchema);
        ducksnode.getDashboards(function(err, dashs, code){
            syncDashboards(dashs, db, function(){
                process.exit(code = 0);
            });
        });
    });
}

var syncDashboards = function(dashs, db, onComplete){
    var data = dashs.data;
    __.each(data, function(elem, key){
        console.log(elem.name, "is on DucksBoard :)");
    });
    var counter = 0;
    __.each(config.dashboards, function(elem, key){
        Dashboard.find({system: elem.system}, function(err, mydashs){
            if (mydashs.length == 0){
                console.log(elem.name, "is not on MongoDB :(");
                console.log(elem.name, "saving to Ducksboard...");
                ducksnode.addDashboard(elem.name, elem.background, function(err, response, code){
                    if (!err){
                        console.log(elem.name, "saved to Ducksboard...");
                        var dash = new Dashboard({
                            name: response.name,
                            background: response.background,
                            slug: response.slug,
                            system: elem.system
                        });
                        dash.save(function(err, d){
                            console.log("Saved", response.name, "into db");
                            console.log("Time to add widgets for this...");
                            addWidgets(response.slug, function(id, system, type, err, body, code){
                                var slots = [];
                                __.each(body.slots, function(elem, key){
                                    slots.push(new Slot({
                                        ducksid: elem.label,
                                        subtitle: elem.subtitle
                                    }));
                                });
                                console.log(body);
                                var widget = new Widget({
                                    ducksid: body.widget.id,
                                    kind: body.widget.kind,
                                    dashboard: body.widget.dashboard,
                                    title: body.widget.title,
                                    slots: slots,
                                    id: id,
                                    system: system,
                                    type: type
                                });
                                widget.save(function(err, w){
                                    console.log("Saved", w, "woooo!");
                                });
                            });
                        });
                    } else {
                        console.log(response, code);
                    }
                });
            } else {
                console.log(elem.name, "is also on MongoDB :)");
            }
        });
    });
}

var readConfig = function(callback){
    console.log("Reading config file");
    fs.readFile('config.json', 'utf8', function(err, data){
    if (err)
        return console.log(err);
    config = JSON.parse(data);
    callback();
    });
}

var addWidgets = function(dashboard, callback){
    __.each(config.widgets, function(widget){
        if (widget.repeat_per_system){
            __.each(config.allowedSys, function(sys, idx){
                if (sys.display){
                    ducksnode.addWidget(widget.kind, widget.name, dashboard, false, widget.slots, sys.name, function(err, body, code){
                        if (sys.system == 'main')
                            type = 'aggregated';
                        else
                            type = widget.type;
                        callback(widget.id, sys.system, type, err, body, code);
                    });
                }
            });
        } else {
            ducksnode.addWidget(widget.kind, widget.name, dashboard, false, widget.slots, '',function(err, body, code){
                callback(widget.id, '', 'general', err, body, code);
            });
        }
    });
}

readConfig(initApp);