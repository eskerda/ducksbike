var mongoose = require('mongoose');

var slotSchema = new mongoose.Schema({
    ducksid: Number,
    title: String,
    id: String
});

var widgetSchema = new mongoose.Schema({
    ducksid: Number,
    kind: String,
    title: String,
    dashboard: String,
    sound: Boolean,
    row: Number,
    column: Number,
    slots: [slotSchema],
    id: String,
    system: String,
    type: String
});


var dashboardSchema = new mongoose.Schema({
    name: String,
    system: String,
    slug: String,
    background: String
});

dashboardSchema.methods.getWidgets = function(Widget, cb){
    Widget.find({
        dashboard: this.slug
    }, cb);
};


module.exports.widgetSchema = widgetSchema;
module.exports.slotSchema = slotSchema;
module.exports.dashboardSchema = dashboardSchema;