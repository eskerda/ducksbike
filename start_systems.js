var __ = require('underscore'),
    fs = require('fs'),
    Ducksnode = require('./lib/ducksnode'),
    mongoose = require('mongoose'),
    schemas = require('./schemas'),
    CityBikes = require('./lib/citybikes');

var StupidCache = function(){
    this.cache = {};

    this.addValue = function(key, shit){
        this.cache[key] = shit;
    }

    this.getValue = function(key){
        if (this.cache[key] === undefined)
            return false;
        return this.cache[key];
    }
}


var ducksnode;
var db;

var Dashboard, Widget, Slot;
var bender = new StupidCache();


var initModels = function(){
    Dashboard = db.model('Dashboard', schemas.dashboardSchema);
    Widget = db.model('Widget', schemas.widgetSchema);
    Slot = db.model('Slot', schemas.slotSchema);
}

var initDB = function(callback){
    db = mongoose.createConnection(config.mongo_uri);
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function(){
        initModels();
        callback();
    });
}

var widgetFunctions = {
    'bikes_taken': function(system, widget, oldStation, station){
        var freeDiff = station.free - oldStation.free;
        if (freeDiff > 0){
            ducksnode.push(widget.slots[0].ducksid, {
                "delta": freeDiff
            }, function(err, code){

            });
        }
    },
    'bikes_left': function(system, widget, oldStation, station){
        var bikeDiff = station.bikes - oldStation.bikes;
        if (bikeDiff > 0){
            ducksnode.push(widget.slots[0].ducksid, {
                "delta": bikeDiff
            }, function(err, code){

            });
        }
    },
    'timer': function(widget){
        ducksnode.push(widget.slots[0].ducksid, {
            "delta": 1
        }, function(err, code){

        });
    },
    'total_stations': function(widget, stations){
        ducksnode.push(widget.slots[0].ducksid, {
            "delta": stations.length
        }, function(err, code){

        });
    }
}

var networksToPool = [];

var filterNetworks = function(callback){
    console.log("Filtering networks > 50");
    CityBikes.networks(function(systems){
        var nSys = systems.length;
        __.each(systems, function(system, idx){
            setTimeout(function(){
                CityBikes.stations(system.name, function(stations){
                    if (stations.length > 50)
                        networksToPool.push(system.name);
                    countStations(stations);
                }, function(){

                });
                nSys--;
                if (nSys == 0)
                    callback();
                console.log('Left..',nSys);
            }, 100 * idx)
        });
    });
};

var poolNetworks = function(){
    __.each(networksToPool, function(sys){
                setInterval(function(){
                    CityBikes.stations(sys, function(stations){
                        analizeSystem(sys, stations);
                    });
                }, 2000);
            });
}

var mainWidgets;

var initApp = function(){
    ducksnode = Ducksnode.create({
        api_key: config.ducks_key
    });

    initDB(function(){
        console.log("everything online");
        
        // Type general are hardtyped

        // Type aggregated recieve pooled callbacks
        Widget.find({type: 'aggregated'}, function(err, widgets){
            mainWidgets = widgets;
        });
        resetStations(function(){
            // Type pooled are pooled
            filterNetworks(function(){
                poolNetworks();
            });
        });
        setInterval(function(){
            updateTimer();
        }, 1000);
    });
}



var analizeSystem = function(system, stations){
    console.log("analizing", system, stations.length);
    oldInfo = bender.getValue(system);
    if (!oldInfo){
        bender.addValue(system, stations);
        return;
    }
    bender.addValue(system, stations);
    var indexes = [];
    var differences = __.filter(stations, function(station, idx){
        if (oldInfo[idx].bikes != station.bikes
            ||
            oldInfo[idx].free != station.free){
            indexes.push(idx);
            return true;
        }
        else
            return false;
    });
    Widget.find({type: 'pooled', system: system}, function(err, widgets){
        __.each(differences, function(station, idx){
            __.each(widgets, function(widget){
                if (widgetFunctions[widget.id] !== undefined){
                    widgetFunctions[widget.id](system, widget, oldInfo[idx], station);
                }
            });
            __.each(mainWidgets, function(widget){
                if (widgetFunctions[widget.id] !== undefined){
                    widgetFunctions[widget.id](system, widget, oldInfo[idx], station);
                }
            });
        });
    });
}

var readConfig = function(callback){
    console.log("Reading config file");
    fs.readFile('config.json', 'utf8', function(err, data){
    if (err)
        return console.log(err);
    config = JSON.parse(data);
    callback();
    });
}

var countStations = function(stations){
    Widget.find({'id': 'total_stations'}, function(err, widgets){
        var w = widgets[0];
        widgetFunctions['total_stations'](w, stations)
    });
}

var resetStations = function(callback){
    Widget.find({'id': 'total_stations'}, function(err, widgets){
        var w = widgets[0];
        ducksnode.push(w.slots[0].ducksid, {
            "value": 0
        }, function(err, code){
            callback();
        });
    });
}

var updateTimer = function(){
    Widget.find({'id': 'timer'}, function(err, widgets){
        var w = widgets[0];
        widgetFunctions['timer'](w);
    });
}
readConfig(initApp);